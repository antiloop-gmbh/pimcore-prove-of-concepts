# ACME Antiloop Academy
Demo project for [Antiloop](https://antiloop.com)

## Get started without any prior setup
To get started you copy the .example files and update their settings according to your setup

    cp .env.example .env
    cp .env.local.example .env.local

Should you be running this the first time and using the docker environment you must do the following steps

    # Connect to the php docker
    docker-compose exec php-fpm bash
    composer install
    ./vendor/bin/pimcore-install --admin-username admin --admin-password admin --mysql-username pimcore --mysql-password pimcore --mysql-database pimcore --mysql-host-socket database --no-interaction


